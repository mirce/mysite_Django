from django import forms
from django.core.files.base import ContentFile
from slugify import slugify
from urllib import request
from .models import Image

class ImageForm(forms.ModelForm):
    class Meta:
        model=Image
        fields=('title','url','description')

    # 处理url字段
    def clean_url(self):
        url=self.cleaned_data['url']
        # 规定图片格式
        valid_extensions=['jpg','jpeg','png']
        # 解析图片路径，查看图片是否符合格式要求
        extension=url.rsplit('.',1)[1].lower()
        if extension not in valid_extensions:
            raise forms.ValidationError("The given Url does not match valid image extension")
        return url

    # 将表单提交数据保存至数据库
    def save(self, force_insert=False,force_update=False,commit=True):
        image=super(ImageForm, self).save(commit=False)
        image_url=self.clean_data['url']
        image_name='{0}.{1}'.format(slugify(image.title),image_url.rsplit('.',1)[1].lower())
        response=request.urlopen(image_url)
        image.image.save(image_name,ContentFile(response.read()),save=False)
        if commit:
            image.save()

        return image
